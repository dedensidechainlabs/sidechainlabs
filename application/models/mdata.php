<?php
class Mdata extends CI_Model
{
	
    function getUsers() 
    {
        $this->db->from('users');
        $this->db->order_by('created_at','DESC');
        $query = $this->db->get();
        return $query->result();
        
    }

    function getVideo() 
    {
        $this->db->from('video');
        $this->db->where('video.publish','1');
        $this->db->order_by('video.created_at','DESC');
        $query = $this->db->get();
        return $query->result();
    }

    function getVideoHome() 
    {
        $this->db->select('video.id as videoid, users.id as userid, video.name as videoname,users.name as username, video.created_at as video_created_at, users.created_at as user_created_at, video.*, users.*');
        $this->db->from('video');
        $this->db->where('video.publish','1');
        $this->db->join('users', 'video.user_id = users.id');
        $this->db->order_by('video.created_at','DESC');
        $this->db->limit('6');
        $query = $this->db->get();
        return $query->result();
    }

    function getVideoHome1() 
    {
        $this->db->select('video.id as videoid, users.id as userid, video.name as videoname,users.name as username, video.created_at as video_created_at, users.created_at as user_created_at, video.*, users.*');
        $this->db->from('video');
        $this->db->join('users', 'video.user_id = users.id');
        $this->db->where('video.section','scene1');
        $this->db->where('video.publish','1');
        $this->db->order_by('video.created_at','DESC');
        $this->db->limit('3');
        $query = $this->db->get();
        return $query->result();
    }

    function getVideoHome2() 
    {
        $this->db->select('video.id as videoid, users.id as userid, video.name as videoname,users.name as username, video.created_at as video_created_at, users.created_at as user_created_at, video.*, users.*');
        $this->db->from('video');
        $this->db->join('users', 'video.user_id = users.id');
        $this->db->where('video.section','scene2');
        $this->db->where('video.publish','1');
        $this->db->order_by('video.created_at','DESC');
        $this->db->limit('3');
        $query = $this->db->get();
        return $query->result();
    }

    function getVideoHome3() 
    {
        $this->db->select('video.id as videoid, users.id as userid, video.name as videoname,users.name as username, video.created_at as video_created_at, users.created_at as user_created_at, video.*, users.*');
        $this->db->from('video');
        $this->db->join('users', 'video.user_id = users.id');
        $this->db->where('video.section','scene3');
        $this->db->where('video.publish','1');
        $this->db->order_by('video.created_at','DESC');
        $this->db->limit('3');
        $query = $this->db->get();
        return $query->result();
    }

    function getVideoScene($scene) 
    {
        $this->db->select('video.id as videoid, users.id as userid, video.name as videoname,users.name as username, video.created_at as video_created_at, users.created_at as user_created_at, video.*, users.*');
        $this->db->from('video');
        $this->db->join('users', 'video.user_id = users.id');
        $this->db->where('section',$scene);
        $this->db->where('video.publish','1');
        $this->db->order_by('video.created_at','DESC');
        $this->db->limit('6');
        $query = $this->db->get();
        return $query->result();
    }

    function getAllVideoScene($scene) 
    {
        $this->db->select('video.id as videoid, users.id as userid, video.name as videoname,users.name as username, video.created_at as video_created_at, users.created_at as user_created_at, video.*, users.*');
        $this->db->from('video');
        $this->db->join('users', 'video.user_id = users.id');
        $this->db->where('section',$scene);
        $this->db->where('video.publish','1');
        $this->db->order_by('video.created_at','DESC');
        $query = $this->db->get();
        return $query->result();
    }

    public function getAllVideoScenePage($scene, $limit, $start) {
        $this->db->select('video.id as videoid, users.id as userid, video.name as videoname,users.name as username, video.created_at as video_created_at, users.created_at as user_created_at, video.*, users.*');
        $this->db->from('video');
        $this->db->join('users', 'video.user_id = users.id');
        $this->db->limit($limit, $start);
        $this->db->where('video.section',$scene);
        $this->db->where('video.publish','1');
        $this->db->order_by('video.created_at','DESC');
        $query = $this->db->get();
        return $query->result();
    }

    function getSearchVideo($txtsearch) 
    {
        $this->db->select('video.id as videoid, users.id as userid, video.name as videoname,users.name as username, video.created_at as video_created_at, users.created_at as user_created_at, video.*, users.*');
        $this->db->from('video');
        $this->db->join('users', 'video.user_id = users.id');
        $this->db->where('video.publish','1');
        $this->db->like('video.name', $txtsearch); 
        $this->db->order_by('video.created_at','DESC');
        $query = $this->db->get();
        return $query->result();
    }

    public function getSearch($txtsearch, $limit, $start) {
        $this->db->select('video.id as videoid, users.id as userid, video.name as videoname,users.name as username, video.created_at as video_created_at, users.created_at as user_created_at, video.*, users.*');
        $this->db->from('video');
        $this->db->join('users', 'video.user_id = users.id');
        $this->db->limit($limit, $start);
        $this->db->where('video.publish','1');
        $this->db->like('video.name', $txtsearch); 
        $this->db->order_by('video.created_at','DESC');
        $query = $this->db->get();
        return $query->result();
    }

    function getVideos() 
    {
        $this->db->select('video.id as videoid, users.id as userid, video.name as videoname,users.name as username, video.created_at as video_created_at, users.created_at as user_created_at, video.*, users.*');
        $this->db->from('video');
        $this->db->join('users', 'video.user_id = users.id');
        $this->db->order_by('video.created_at','DESC');
        $query = $this->db->get();
        return $query->result();
    }

     function getDetailVideo($videoid) 
    {
        $this->db->select('video.id as videoid, users.id as userid, video.name as videoname,users.name as username, video.created_at as video_created_at, users.created_at as user_created_at, video.*, users.*');
        $this->db->from('video');
        $this->db->join('users', 'video.user_id = users.id');
        $this->db->where('video.id',$videoid);
        //$this->db->order_by('video.created_at','DESC');
        //$this->db->select('video.name, users.name, *');
        $query = $this->db->get();
        return $query->result();
    }

    function getVideoDetail($id) 
    {
        $this->db->select('video.id as videoid, users.id as userid, video.name as videoname,users.name as username, video.created_at as video_created_at, users.created_at as user_created_at, video.*, users.*');
        $this->db->from('video');
        $this->db->join('users', 'video.user_id = users.id');
        $this->db->where('video.user_id',$id);
        //$this->db->order_by('video.created_at','DESC');
        //$this->db->select('video.name, users.name, *');
        $query = $this->db->get();
		return $query->result();
    }


    function getVideoDetailByVideoID($id) 
    {
       
        $this->db->from('video');
        $this->db->where('id',$id);
        $this->db->order_by('video.created_at','DESC');
        $query = $this->db->get();
        return $query->result();
    }
    
    function saveMaintenanceMode($settingname, $settingvalue)
    {
        $this->db->where('setting', $settingname);
        $result = $this->db->update('config', $settingvalue); 
        return $result;
    }

    function updateVideo($id, $dataVideo)
    {
        $this->db->where('id', $id);
        $result = $this->db->update('video', $dataVideo); 
        return $result;
    }

    function updateSetting($setting, $value)
    {
        $this->db->where('setting', $setting);
        $result = $this->db->update('config', $value); 
        return $result;
    }	

    function getPrize()
    {
        $this->db->from('config');
        $this->db->where('setting','prize');
        $query = $this->db->get();
        return $query->result();
    }

    function getTnc()
    {
        $this->db->from('config');
        $this->db->where('setting','tnc');
        $query = $this->db->get();
        return $query->result();
    }

    function getPvp()
    {
        $this->db->from('config');
        $this->db->where('setting','pvp');
        $query = $this->db->get();
        return $query->result();
    }
}
?>
