<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Site extends CI_Controller {

	function __construct()
    {
        parent::__construct();
        $this->load->model('mdata');
		$this->load->helper(array('form', 'url','file'));
		date_default_timezone_set('Asia/Jakarta'); 
	}	

	public function index()
	{
		$data['title'] = "Sidechain Labs - The Digital Artventures";
		$this->load->view('vindex',$data);
	}
	
}