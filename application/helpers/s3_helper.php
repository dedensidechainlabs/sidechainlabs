<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Base URL
 * 
 * Create a local URL based on your basepath.
 * Segments can be passed in as a string or an array, same as site_url
 * or a URL to a file can be passed in, e.g. to an image file.
 *
 * @access	public
 * @param string
 * @return	string
 */
if ( ! function_exists('s3_url'))
{
	function s3_url($uri = '')
	{
		$CI =& get_instance();
		return $CI->config->item('s3_url').$uri;
	}
}