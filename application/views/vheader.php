<!DOCTYPE html>
<html lang="en-US" class="csstransforms csstransforms3d csstransitions js_active  vc_desktop  vc_transform  vc_transform ">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="UTF-8">
    <meta name="HandheldFriendly" content="true">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <title><?php echo $title; ?></title>

    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="http://demo.stylishthemes.co/clubix/light/xmlrpc.php">

    <!-- Script required for extra functionality on the comment form -->
    
    <link rel="alternate" type="application/rss+xml" title="Clubix – Nightlife, Artists, Music &amp; Events WordPress Theme » Feed" href="http://demo.stylishthemes.co/clubix/light/feed/">
	<link rel="alternate" type="application/rss+xml" title="Clubix – Nightlife, Artists, Music &amp; Events WordPress Theme » Comments Feed" href="http://demo.stylishthemes.co/clubix/light/comments/feed/">
	<link rel="stylesheet" id="contact-form-7-css" href="<?php echo base_url(); ?>assets/css/styles.css" type="text/css" media="all">
	<link rel="stylesheet" id="rs-plugin-settings-css" href="<?php echo base_url(); ?>assets/css/settings.css" type="text/css" media="all">
	<style type="text/css">
	.tp-caption.medium_grey{position:absolute;color:#fff;text-shadow:0px 2px 5px rgba(0,0,0,0.5);font-weight:700;font-size:20px;line-height:20px;font-family:Arial;padding:2px 4px;margin:0px;border-width:0px;border-style:none;background-color:#888;white-space:nowrap}.tp-caption.small_text{position:absolute;color:#fff;text-shadow:0px 2px 5px rgba(0,0,0,0.5);font-weight:700;font-size:14px;line-height:20px;font-family:Arial;margin:0px;border-width:0px;border-style:none;white-space:nowrap}.tp-caption.medium_text{position:absolute;color:#fff;text-shadow:0px 2px 5px rgba(0,0,0,0.5);font-weight:700;font-size:20px;line-height:20px;font-family:Arial;margin:0px;border-width:0px;border-style:none;white-space:nowrap}.tp-caption.large_text{position:absolute;color:#fff;text-shadow:0px 2px 5px rgba(0,0,0,0.5);font-weight:700;font-size:40px;line-height:40px;font-family:Arial;margin:0px;border-width:0px;border-style:none;white-space:nowrap}.tp-caption.very_large_text{position:absolute;color:#fff;text-shadow:0px 2px 5px rgba(0,0,0,0.5);font-weight:700;font-size:60px;line-height:60px;font-family:Arial;margin:0px;border-width:0px;border-style:none;white-space:nowrap;letter-spacing:-2px}.tp-caption.very_big_white{position:absolute;color:#fff;text-shadow:none;font-weight:800;font-size:60px;line-height:60px;font-family:Arial;margin:0px;border-width:0px;border-style:none;white-space:nowrap;padding:0px 4px;padding-top:1px;background-color:#000}.tp-caption.very_big_black{position:absolute;color:#000;text-shadow:none;font-weight:700;font-size:60px;line-height:60px;font-family:Arial;margin:0px;border-width:0px;border-style:none;white-space:nowrap;padding:0px 4px;padding-top:1px;background-color:#fff}.tp-caption.modern_medium_fat{position:absolute;color:#000;text-shadow:none;font-weight:800;font-size:24px;line-height:20px;font-family:"Open Sans",sans-serif;margin:0px;border-width:0px;border-style:none;white-space:nowrap}.tp-caption.modern_medium_fat_white{position:absolute;color:#fff;text-shadow:none;font-weight:800;font-size:24px;line-height:20px;font-family:"Open Sans",sans-serif;margin:0px;border-width:0px;border-style:none;white-space:nowrap}.tp-caption.modern_medium_light{position:absolute;color:#000;text-shadow:none;font-weight:300;font-size:24px;line-height:20px;font-family:"Open Sans",sans-serif;margin:0px;border-width:0px;border-style:none;white-space:nowrap}.tp-caption.modern_big_bluebg{position:absolute;color:#fff;text-shadow:none;font-weight:800;font-size:30px;line-height:36px;font-family:"Open Sans",sans-serif;padding:3px 10px;margin:0px;border-width:0px;border-style:none;background-color:#4e5b6c;letter-spacing:0}.tp-caption.modern_big_redbg{position:absolute;color:#fff;text-shadow:none;font-weight:300;font-size:30px;line-height:36px;font-family:"Open Sans",sans-serif;padding:3px 10px;padding-top:1px;margin:0px;border-width:0px;border-style:none;background-color:#de543e;letter-spacing:0}.tp-caption.modern_small_text_dark{position:absolute;color:#555;text-shadow:none;font-size:14px;line-height:22px;font-family:Arial;margin:0px;border-width:0px;border-style:none;white-space:nowrap}.tp-caption.boxshadow{-moz-box-shadow:0px 0px 20px rgba(0,0,0,0.5);-webkit-box-shadow:0px 0px 20px rgba(0,0,0,0.5);box-shadow:0px 0px 20px rgba(0,0,0,0.5)}.tp-caption.black{color:#000;text-shadow:none}.tp-caption.noshadow{text-shadow:none}.tp-caption.thinheadline_dark{position:absolute;color:rgba(0,0,0,0.85);text-shadow:none;font-weight:300;font-size:30px;line-height:30px;font-family:"Open Sans";background-color:transparent}.tp-caption.thintext_dark{position:absolute;color:rgba(0,0,0,0.85);text-shadow:none;font-weight:300;font-size:16px;line-height:26px;font-family:"Open Sans";background-color:transparent}.tp-caption.largeblackbg{position:absolute;color:#fff;text-shadow:none;font-weight:300;font-size:50px;line-height:70px;font-family:"Open Sans";background-color:#000;padding:0px 20px;-webkit-border-radius:0px;-moz-border-radius:0px;border-radius:0px}.tp-caption.largepinkbg{position:absolute;color:#fff;text-shadow:none;font-weight:300;font-size:50px;line-height:70px;font-family:"Open Sans";background-color:#db4360;padding:0px 20px;-webkit-border-radius:0px;-moz-border-radius:0px;border-radius:0px}.tp-caption.largewhitebg{position:absolute;color:#000;text-shadow:none;font-weight:300;font-size:50px;line-height:70px;font-family:"Open Sans";background-color:#fff;padding:0px 20px;-webkit-border-radius:0px;-moz-border-radius:0px;border-radius:0px}.tp-caption.largegreenbg{position:absolute;color:#fff;text-shadow:none;font-weight:300;font-size:50px;line-height:70px;font-family:"Open Sans";background-color:#67ae73;padding:0px 20px;-webkit-border-radius:0px;-moz-border-radius:0px;border-radius:0px}.tp-caption.excerpt{font-size:36px;line-height:36px;font-weight:700;font-family:Arial;color:#ffffff;text-decoration:none;background-color:rgba(0,0,0,1);text-shadow:none;margin:0px;letter-spacing:-1.5px;padding:1px 4px 0px 4px;width:150px;white-space:normal !important;height:auto;border-width:0px;border-color:rgb(255,255,255);border-style:none}.tp-caption.large_bold_grey{font-size:60px;line-height:60px;font-weight:800;font-family:"Open Sans";color:rgb(102,102,102);text-decoration:none;background-color:transparent;text-shadow:none;margin:0px;padding:1px 4px 0px;border-width:0px;border-color:rgb(255,214,88);border-style:none}.tp-caption.medium_thin_grey{font-size:34px;line-height:30px;font-weight:300;font-family:"Open Sans";color:rgb(102,102,102);text-decoration:none;background-color:transparent;padding:1px 4px 0px;text-shadow:none;margin:0px;border-width:0px;border-color:rgb(255,214,88);border-style:none}.tp-caption.small_thin_grey{font-size:18px;line-height:26px;font-weight:300;font-family:"Open Sans";color:rgb(117,117,117);text-decoration:none;background-color:transparent;padding:1px 4px 0px;text-shadow:none;margin:0px;border-width:0px;border-color:rgb(255,214,88);border-style:none}.tp-caption.lightgrey_divider{text-decoration:none;background-color:rgba(235,235,235,1);width:370px;height:3px;background-position:initial initial;background-repeat:initial initial;border-width:0px;border-color:rgb(34,34,34);border-style:none}.tp-caption.large_bold_darkblue{font-size:58px;line-height:60px;font-weight:800;font-family:"Open Sans";color:rgb(52,73,94);text-decoration:none;background-color:transparent;border-width:0px;border-color:rgb(255,214,88);border-style:none}.tp-caption.medium_bg_darkblue{font-size:20px;line-height:20px;font-weight:800;font-family:"Open Sans";color:rgb(255,255,255);text-decoration:none;background-color:rgb(52,73,94);padding:10px;border-width:0px;border-color:rgb(255,214,88);border-style:none}.tp-caption.medium_bold_red{font-size:24px;line-height:30px;font-weight:800;font-family:"Open Sans";color:rgb(227,58,12);text-decoration:none;background-color:transparent;padding:0px;border-width:0px;border-color:rgb(255,214,88);border-style:none}.tp-caption.medium_light_red{font-size:21px;line-height:26px;font-weight:300;font-family:"Open Sans";color:rgb(227,58,12);text-decoration:none;background-color:transparent;padding:0px;border-width:0px;border-color:rgb(255,214,88);border-style:none}.tp-caption.medium_bg_red{font-size:20px;line-height:20px;font-weight:800;font-family:"Open Sans";color:rgb(255,255,255);text-decoration:none;background-color:rgb(227,58,12);padding:10px;border-width:0px;border-color:rgb(255,214,88);border-style:none}.tp-caption.medium_bold_orange{font-size:24px;line-height:30px;font-weight:800;font-family:"Open Sans";color:rgb(243,156,18);text-decoration:none;background-color:transparent;border-width:0px;border-color:rgb(255,214,88);border-style:none}.tp-caption.medium_bg_orange{font-size:20px;line-height:20px;font-weight:800;font-family:"Open Sans";color:rgb(255,255,255);text-decoration:none;background-color:rgb(243,156,18);padding:10px;border-width:0px;border-color:rgb(255,214,88);border-style:none}.tp-caption.grassfloor{text-decoration:none;background-color:rgba(160,179,151,1);width:4000px;height:150px;border-width:0px;border-color:rgb(34,34,34);border-style:none}.tp-caption.large_bold_white{font-size:58px;line-height:60px;font-weight:800;font-family:"Open Sans";color:rgb(255,255,255);text-decoration:none;background-color:transparent;border-width:0px;border-color:rgb(255,214,88);border-style:none}.tp-caption.medium_light_white{font-size:30px;line-height:36px;font-weight:300;font-family:"Open Sans";color:rgb(255,255,255);text-decoration:none;background-color:transparent;padding:0px;border-width:0px;border-color:rgb(255,214,88);border-style:none}.tp-caption.mediumlarge_light_white{font-size:34px;line-height:40px;font-weight:300;font-family:"Open Sans";color:rgb(255,255,255);text-decoration:none;background-color:transparent;padding:0px;border-width:0px;border-color:rgb(255,214,88);border-style:none}.tp-caption.mediumlarge_light_white_center{font-size:34px;line-height:40px;font-weight:300;font-family:"Open Sans";color:#ffffff;text-decoration:none;background-color:transparent;padding:0px 0px 0px 0px;text-align:center;border-width:0px;border-color:rgb(255,214,88);border-style:none}.tp-caption.medium_bg_asbestos{font-size:20px;line-height:20px;font-weight:800;font-family:"Open Sans";color:rgb(255,255,255);text-decoration:none;background-color:rgb(127,140,141);padding:10px;border-width:0px;border-color:rgb(255,214,88);border-style:none}.tp-caption.medium_light_black{font-size:30px;line-height:36px;font-weight:300;font-family:"Open Sans";color:rgb(0,0,0);text-decoration:none;background-color:transparent;padding:0px;border-width:0px;border-color:rgb(255,214,88);border-style:none}.tp-caption.large_bold_black{font-size:58px;line-height:60px;font-weight:800;font-family:"Open Sans";color:rgb(0,0,0);text-decoration:none;background-color:transparent;border-width:0px;border-color:rgb(255,214,88);border-style:none}.tp-caption.mediumlarge_light_darkblue{font-size:34px;line-height:40px;font-weight:300;font-family:"Open Sans";color:rgb(52,73,94);text-decoration:none;background-color:transparent;padding:0px;border-width:0px;border-color:rgb(255,214,88);border-style:none}.tp-caption.small_light_white{font-size:17px;line-height:28px;font-weight:300;font-family:"Open Sans";color:rgb(255,255,255);text-decoration:none;background-color:transparent;padding:0px;border-width:0px;border-color:rgb(255,214,88);border-style:none}.tp-caption.roundedimage{border-width:0px;border-color:rgb(34,34,34);border-style:none}.tp-caption.large_bg_black{font-size:40px;line-height:40px;font-weight:800;font-family:"Open Sans";color:rgb(255,255,255);text-decoration:none;background-color:rgb(0,0,0);padding:10px 20px 15px;border-width:0px;border-color:rgb(255,214,88);border-style:none}.tp-caption.mediumwhitebg{font-size:30px;line-height:30px;font-weight:300;font-family:"Open Sans";color:rgb(0,0,0);text-decoration:none;background-color:rgb(255,255,255);padding:5px 15px 10px;text-shadow:none;border-width:0px;border-color:rgb(0,0,0);border-style:none}.tp-caption.largepinkbg-02{color:rgb(255,255,255);font-size:18px;line-height:41px;font-weight:500;font-family:"Open Sans";text-decoration:none;padding:0px 20px;text-shadow:none;text-transform:uppercase;background-color:rgba(142,68,173,0.498039);border-width:0px;border-color:rgb(255,255,255);border-style:none}.tp-caption.largeblackbg-02{color:rgb(255,255,255);text-shadow:none;font-weight:500;font-size:18px;line-height:40px;font-family:"Open Sans";padding:0px 20px;text-decoration:none;text-transform:uppercase;background-color:rgba(0,0,0,0.6);border-width:0px;border-color:rgb(255,255,255);border-style:none}.tp-caption.largepinkbg-03{color:rgb(255,255,255);font-size:24px;line-height:50px;font-weight:500;font-family:"Open Sans";text-decoration:none;padding:0px 20px;text-shadow:none;text-transform:uppercase;background-color:rgba(142,68,173,0.498039);border-width:0px;border-color:rgb(255,255,255);border-style:none}.tp-caption.largeblackbg-03{font-size:18px;line-height:42px;font-weight:500;font-family:"Open Sans";color:#ffffff;text-decoration:none;padding:0px 15px 0px 15px;text-shadow:none;background-color:rgba(0,0,0,0.6);border-width:0px;border-color:rgb(255,255,255);border-style:none}
		.tp-caption a{color:#fff;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.clx-btn-slider{padding:15px 21px;  border:2px solid #d0cdce;  text-transform:uppercase; color:#fff !important; font-size:16px; font-weight:700}.clx-btn-slider:hover{background:rgba(0,0,0,0.6)}
	</style>
	<link rel="stylesheet" id="master-css" href="<?php echo base_url(); ?>assets/css/master.css" type="text/css" media="all">
	<link rel="stylesheet" id="new-css" href="<?php echo base_url(); ?>assets/css/new.css" type="text/css" media="all">
	<link rel="stylesheet" id="style-css" href="<?php echo base_url(); ?>assets/css/style.css" type="text/css" media="all">
	<div class="fit-vids-style" id="fit-vids-style" style="display: none;">­<style>.fluid-width-video-wrapper{width:100%;position:relative;padding:0;}.fluid-width-video-wrapper iframe,.fluid-width-video-wrapper object,.fluid-width-video-wrapper embed {position:absolute;top:0;left:0;width:100%;height:100%;}</style></div><script src="<?php echo base_url(); ?>assets/js/froogaloop2.min.js"></script><script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.js"></script><style type="text/css"></style>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-migrate.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.themepunch.plugins.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.themepunch.revolution.min.js"></script>
	<link rel="EditURI" type="application/rsd+xml" title="RSD" href="http://demo.stylishthemes.co/clubix/light/xmlrpc.php?rsd">
	<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="http://demo.stylishthemes.co/clubix/light/wp-includes/wlwmanifest.xml"> 
	<meta name="generator" content="WordPress 4.0.1">
	<meta name="generator" content="WooCommerce 2.1.12">
	<link rel="canonical" href="<?php echo base_url(); ?>assets/css/Clubix – Nightlife, Artists, Music & Events WordPress Theme.html">
	<link rel="shortlink" href="<?php echo base_url(); ?>assets/css/Clubix – Nightlife, Artists, Music & Events WordPress Theme.html">
	<meta name="generator" content="Powered by Visual Composer - drag and drop page builder for WordPress.">
	<style type="text/css" title="dynamic-css" class="options-output">body{background-repeat:no-repeat;background-size:cover;background-attachment:fixed;background-position:center center;background-image:url('<?php echo base_url(); ?>assets/img/bg-default.jpg');}</style>
	    
	    <style type="text/css">

	    a {
	        color: rgba(224,15,44,0.99);
	        text-decoration: none;
	    }
	    a:hover,
	    a:focus {
	        color: rgba(224,15,44,0.99);
	    }
	    .text-primary {
	        color: rgba(224,15,44,0.99);
	    }
	    .bg-primary {
	        background-color: rgba(224,15,44,0.99);
	    }
	    blockquote {
	        border-left: 4px solid rgba(224,15,44,0.99);
	    }
	    .btn-primary {
	        background-color: rgba(224,15,44,0.99);
	    }
	    .btn-primary:hover,
	    .btn-primary:focus,
	    .btn-primary:active,
	    .btn-primary.active,
	    .open > .dropdown-toggle.btn-primary {
	        background-color: rgba(224,15,44,0.99);
	    }
	    .btn-primary.disabled,
	    .btn-primary[disabled],
	    fieldset[disabled] .btn-primary,
	    .btn-primary.disabled:hover,
	    .btn-primary[disabled]:hover,
	    fieldset[disabled] .btn-primary:hover,
	    .btn-primary.disabled:focus,
	    .btn-primary[disabled]:focus,
	    fieldset[disabled] .btn-primary:focus,
	    .btn-primary.disabled:active,
	    .btn-primary[disabled]:active,
	    fieldset[disabled] .btn-primary:active,
	    .btn-primary.disabled.active,
	    .btn-primary[disabled].active,
	    fieldset[disabled] .btn-primary.active {
	        background-color: rgba(224,15,44,0.99);
	    }
	    .btn-primary .badge {
	        color: rgba(224,15,44,0.99);
	    }
	    .btn-link {
	        color: rgba(224,15,44,0.99);
	    }
	    .btn-link:hover,
	    .btn-link:focus {
	        color: rgba(224,15,44,0.99);
	    }
	    .dropdown-menu > .active > a,
	    .dropdown-menu > .active > a:hover,
	    .dropdown-menu > .active > a:focus {
	        background-color: rgba(224,15,44,0.99);
	    }
	    .dropdown > a {
	        background: rgba(224,15,44,0.99);
	    }
	    .dropdown .dropdown-menu {
	        border-top: 1px solid rgba(224,15,44,0.99);
	    }
	    .dropdown .dropdown-menu li a {
	        background: rgba(224,15,44,0.99);
	    }
	    .dropdown .dropdown-menu li a i {
	        background: rgba(224,15,44,0.99);
	    }
	    .dropdown .dropdown-menu li:first-child a i {
	        border-top: 1px solid rgba(224,15,44,0.99);
	    }
	    .nav .open > a,
	    .nav .open > a:hover,
	    .nav .open > a:focus {
	        border-color: rgba(224,15,44,0.99);
	    }
	    .nav-tabs > li > a:hover {
	        border-color: rgba(224,15,44,0.99);
	    }
	    .nav-tabs > li.active > a,
	    .nav-tabs > li.active > a:hover,
	    .nav-tabs > li.active > a:focus {
	        border: 1px solid rgba(224,15,44,0.99);
	    }
	    .pagination > li > a:hover,
	    .pagination > li > span:hover,
	    .pagination > li > a:focus,
	    .pagination > li > span:focus {
	        background-color: rgba(224,15,44,0.99);
	    }
	    .pagination > .active > a,
	    .pagination > .active > span,
	    .pagination > .active > a:hover,
	    .pagination > .active > span:hover,
	    .pagination > .active > a:focus,
	    .pagination > .active > span:focus {
	        background-color: rgba(224,15,44,0.99);
	        border-color: rgba(224,15,44,0.99);
	    }
	    .label-primary {
	        background-color: rgba(224,15,44,0.99);
	    }
	    a.list-group-item.active > .badge,
	    .nav-pills > .active > a > .badge {
	        color: rgba(224,15,44,0.99);
	    }
	    a.thumbnail:hover,
	    a.thumbnail:focus,
	    a.thumbnail.active {
	        border-color: rgba(224,15,44,0.99);
	    }
	    .progress-bar {
	        background-color: rgba(224,15,44,0.99);
	    }
	    .list-group-item.active,
	    .list-group-item.active:hover,
	    .list-group-item.active:focus {
	        background-color: rgba(224,15,44,0.99);
	        border-color: rgba(224,15,44,0.99);
	    }
	    .panel a .cs {
	        background: rgba(224,15,44,0.99);
	    }
	    .panel-primary {
	        border-color: rgba(224,15,44,0.99);
	    }
	    .panel-primary > .panel-heading {
	        background-color: rgba(224,15,44,0.99);
	        border-color: rgba(224,15,44,0.99);
	    }
	    .panel-primary > .panel-heading + .panel-collapse > .panel-body {
	        border-top-color: rgba(224,15,44,0.99);
	    }
	    .panel-primary > .panel-footer + .panel-collapse > .panel-body {
	        border-bottom-color: rgba(224,15,44,0.99);
	    }
	    .head-container .menu .navbar .navbar-collapse .nav li.active,
	    .head-container .menu .navbar .navbar-collapse .nav li:hover {
	        border-bottom: 2px solid rgba(224,15,44,0.99);
	    }
	    .head-container .menu .navbar .navbar-collapse .nav li .sub-menu li a:hover {
	        background: rgba(224,15,44,0.99);
	    }
	    .head-container .menu .navbar .navbar-form:hover button {
	        background: rgba(224,15,44,0.99);
	    }
	    .head-container .menu .my-cart-link span {
	        background: rgba(224,15,44,0.99);
	    }
	    @media (max-width: 1030px) {
	        .head-container .menu .navbar .navbar-collapse .nav li.active > a {
	            background: rgba(224,15,44,0.99);
	        }
	        .head-container .menu .navbar .navbar-collapse .nav li a:hover {
	            background: rgba(224,15,44,0.99);
	        }
	        .head-container .menu .navbar .navbar-collapse .nav li .sub-menu li a:hover {
	            background: rgba(224,15,44,0.99);
	        }
	        .head-container .menu .navbar .navbar-toggle:hover,
	        .head-container .menu .navbar .navbar-toggle:focus {
	            background: rgba(224,15,44,0.99);
	        }
	    }
	    .footer {
	        border-top: 3px solid rgba(224,15,44,0.99);
	    }
	    .back-to-top {
	        background: rgba(224,15,44,0.99);
	    }
	    .footer-info .info p a:hover {
	        color: rgba(224,15,44,0.99);
	    }
	    .breadcrumb-container .nav-posts:hover {
	        background: rgba(224,15,44,0.99);
	    }
	    .widget.widget_recent_entries ul li a:before,
	    .widget.widget_meta ul li a:before {
	        color: rgba(224,15,44,0.99);
	    }
	    .widget.widget_recent_entries ul li a:hover,
	    .widget.widget_meta ul li a:hover {
	        color: rgba(224,15,44,0.99);
	    }
	    .widget_search form:hover button {
	        background: rgba(224,15,44,0.99);
	    }
	    .event-widget figure > section > div a,
	    .event-widget-countdown figure > section > div a {
	        background: rgba(224,15,44,0.99);
	    }
	    .event-widget figure > section > div a:hover,
	    .event-widget-countdown figure > section > div a:hover {
	        color: rgba(224,15,44,0.99);
	    }
	    .widget_search:hover .icon {
	        background: rgba(224,15,44,0.99);
	    }
	    .events-widget article .right .buy {
	        background: rgba(224,15,44,0.8);
	    }
	    .events-widget article .right .buy a {
	        color: rgba(224,15,44,0.99);
	    }
	    .events-widget article .right > a {
	        background: rgba(224,15,44,0.99);
	    }
	    .events-widget article .right > a:hover {
	        color: rgba(224,15,44,0.99);
	    }
	    .events-widget article .right h4 a:hover {
	        color: rgba(224,15,44,0.99);
	    }
	    .top-albums-widget article figure ul li a:hover {
	        color: rgba(224,15,44,0.99);
	    }
	    .top-albums-widget article .content > a {
	        border: 1px solid rgba(224,15,44,0.99);
	    }
	    .top-albums-widget article .content > a:hover {
	        color: rgba(224,15,44,0.99);
	    }
	    .top-rated-albums-widget article figure .content h5 a:hover {
	        color: rgba(224,15,44,0.99);
	    }
	    .top-rated-albums-widget article figure .content p a:hover {
	        color: rgba(224,15,44,0.99);
	    }
	    .post-article .thumbnail-article figure ul li a:hover {
	        color: rgba(224,15,44,0.99);
	    }
	    .post-article .content-article .entry-tags ul li a:hover,
	    .post-article .content-event-article .entry-tags ul li a:hover {
	        color: rgba(224,15,44,0.99);
	    }
	    .post-article .content-article .entry-meta span a:hover,
	    .post-article .content-event-article .entry-meta span a:hover {
	        color: rgba(224,15,44,0.99);
	    }
	    .post-article .content-article > a,
	    .post-article .content-event-article > a {
	        background: rgba(224,15,44,0.99);
	    }
	    .post-article .content-article > a:hover,
	    .post-article .content-event-article > a:hover {
	        color: rgba(224,15,44,0.99);
	    }
	    .comment-respond .form-submit input[type=submit] {
	        background: rgba(224,15,44,0.99);
	    }
	    .comment-respond .form-submit input[type=submit]:hover {
	        color: rgba(224,15,44,0.99);
	    }
	    .products > li .figure-product .onsale {
	        background: rgba(224,15,44,0.8);
	    }
	    .products > li .product-buttons a {
	        background: rgba(224,15,44,0.99);
	    }
	    .products > li img {
	        border-bottom: 5px solid rgba(224,15,44,0.99);
	    }
	    .widget_product_search form:hover button[type=submit],
	    .widget_product_search form:hover input[type=submit] {
	        background: rgba(224,15,44,0.99);
	    }
	    .widget_product_search form:hover input[type=text] {
	        border-color: rgba(224,15,44,0.99);
	    }
	    .widget_product_categories .product-categories li a:hover {
	        color: rgba(224,15,44,0.99);
	    }
	    .price_slider_wrapper .price_slider_amount button[type=submit] {
	        background: rgba(224,15,44,0.99);
	    }
	    .price_slider_wrapper .price_slider_amount button[type=submit]:hover {
	        color: rgba(224,15,44,0.99);
	    }
	    .woocommerce-pagination .page-numbers li a:hover,
	    .woocommerce-pagination .page-numbers li span:hover {
	        color: rgba(224,15,44,0.99);
	        border-color: rgba(224,15,44,0.99);
	    }
	    .woocommerce-pagination .page-numbers li span {
	        color: rgba(224,15,44,0.99);
	        border-color: rgba(224,15,44,0.99);
	    }
	    .summary .product-border:before {
	        background: rgba(224,15,44,0.99);
	    }
	    .summary .product_meta > span a:hover {
	        color: rgba(224,15,44,0.99);
	    }
	    .cart button[type=submit] {
	        background: rgba(224,15,44,0.99);
	    }
	    .cart input[type=button]:hover {
	        background: rgba(224,15,44,0.99);
	    }
	    .woocommerce .login input[type=submit],
	    .woocommerce .checkout_coupon input[type=submit],
	    .woocommerce .lost_reset_password input[type=submit],
	    .woocommerce .checkout input[type=submit],
	    .woocommerce form.register input[type=submit] {
	        background: rgba(224,15,44,0.99);
	    }
	    .woocommerce .login input[type=submit]:hover,
	    .woocommerce .checkout_coupon input[type=submit]:hover,
	    .woocommerce .lost_reset_password input[type=submit]:hover,
	    .woocommerce .checkout input[type=submit]:hover,
	    .woocommerce form.register input[type=submit]:hover {
	        color: rgba(224,15,44,0.99);
	    }
	    .woocommerce .cart-totals-container input.checkout-button {
	        background: rgba(224,15,44,0.99);
	    }
	    .woocommerce .cart-totals-container > button.checkout-button {
	        background: rgba(224,15,44,0.99);
	    }
	    .shop-category.man h1 {
	        color: rgba(224,15,44,0.99);
	    }
	    .shop-category.man a {
	        color: rgba(224,15,44,0.99);
	    }
	    .comment-respond form input[type=submit] {
	        background: rgba(224,15,44,0.99);
	    }
	    .comment-respond form input[type=submit]:hover {
	        color: rgba(224,15,44,0.99);
	    }
	    .events-container article figure figcaption .min-info {
	        border-bottom: 5px solid rgba(224,15,44,0.99);
	    }
	    .events-container article figure .main-content > div > a {
	        background: rgba(224,15,44,0.99);
	    }
	    .events-container article figure .main-content > div > a:hover {
	        color: rgba(224,15,44,0.99);
	    }
	    article.simple-event .right > a {
	        background: rgba(224,15,44,0.99);
	    }
	    article.simple-event .right > a:hover {
	        color: rgba(224,15,44,0.99);
	    }
	    article.simple-event .right h4 a:hover {
	        color: rgba(224,15,44,0.99);
	    }
	    article.event-article figure .content .entry-tags ul li a:hover {
	        color: rgba(224,15,44,0.99);
	    }
	    article.event-article figure .content .entry-meta span a:hover {
	        color: rgba(224,15,44,0.99);
	    }
	    article.event-article figure .content > a {
	        background: rgba(224,15,44,0.99);
	    }
	    article.event-article figure .content > a:hover {
	        color: rgba(224,15,44,0.99);
	    }
	    @media (max-width: 768px) {
	        article.simple-event .right > a {
	            background: rgba(224,15,44,0.99);
	        }
	        article.simple-event .right > a:hover {
	            color: rgba(224,15,44,0.99);
	        }
	        article.simple-event .right h4 a:hover {
	            color: rgba(224,15,44,0.99);
	        }
	    }
	    .ablums-posts-right article .left figure ul li a:hover {
	        color: rgba(224,15,44,0.99);
	    }
	    .ablums-posts-right article .left .content > a {
	        border: 1px solid rgba(224,15,44,0.99);
	    }
	    .ablums-posts-right article .left .content > a:hover {
	        color: rgba(224,15,44,0.99);
	    }
	    .ablums-posts-bottom article .left figure ul li a:hover {
	        color: rgba(224,15,44,0.99);
	    }
	    .ablums-posts-bottom article .left .content > a {
	        border: 1px solid rgba(224,15,44,0.99);
	    }
	    .ablums-posts-bottom article .left .content > a:hover {
	        color: rgba(224,15,44,0.99);
	    }
	    .albums-container article figure .back-face,
	    .videos-container article figure .back-face,
	    .photos-container article figure .back-face,
	    .artists-container article figure .back-face {
	        background: rgba(224,15,44,0.8);
	    }
	    .social-list ul li a:hover {
	        background: rgba(224,15,44,0.99);
	    }
	    .underline-bg .underline {
	        background: rgba(224,15,44,0.99);
	    }
	    .categories-portfolio ul li a:hover {
	        background: rgba(224,15,44,0.99);
	    }
	    .minimal-player ul li .time-bar span,
	    .playlist-content ul li .time-bar span {
	        background: rgba(224,15,44,0.8);
	    }
	    .minimal-player ul li:hover > a,
	    .playlist-content ul li:hover > a {
	        background: rgba(224,15,44,0.99);
	    }
	    .minimal-player ul li.active,
	    .playlist-content ul li.active {
	        border-color: rgba(224,15,44,0.99);
	    }
	    .minimal-player ul li.active > a,
	    .playlist-content ul li.active > a {
	        background: rgba(224,15,44,0.99);
	        border-right: 1px solid rgba(224,15,44,0.99);
	    }
	    .base-player {
	        border-top: 2px solid rgba(224,15,44,0.99);
	    }
	    .base-player .content-base-player .buttons .play-pause.pause {
	        background: rgba(224,15,44,0.99);
	    }
	    .base-player .content-base-player .sound-informations {
	        border-right: 1px solid rgba(224,15,44,0.99);
	    }
	    .base-player .content-base-player .sound-bar-container .sound-bar-content span.progress-sound {
	        background: rgba(224,15,44,0.8);
	    }
	    .base-player .content-base-player .playlist .button-playlist {
	        background: rgba(224,15,44,0.99);
	    }
	    figure.feature .bg-feature {
	        background: rgba(224,15,44,0.99);
	    }
	    figure.feature .container-feature .content-feature {
	        background: rgba(224,15,44,0.8);
	    }
	    .top-events-albums {
	        border-bottom: 5px solid rgba(224,15,44,0.99);
	    }
	    .top-events-albums .events-albums ul li figure .main-content {
	        background: rgba(224,15,44,0.8);
	    }
	    .top-events-albums .events-albums ul li figure .main-content > a {
	        color: rgba(224,15,44,0.99);
	    }
	    .top-events-albums .hide-top-events-albums {
	        background: rgba(224,15,44,0.99);
	    }
	    body.light-layout .widget_product_search form:hover button[type=submit],
	    body.light-layout .widget_product_search form:hover input[type=submit] {
	        background: rgba(224,15,44,0.99);
	    }
	    body.light-layout .ablums-posts-right article .left .content > a:hover {
	        color: rgba(224,15,44,0.99);
	    }
	    body.light-layout .ablums-posts-bottom article .left .content > a:hover {
	        color: rgba(224,15,44,0.99);
	    }
	    body.light-layout .breadcrumb-container .nav-posts:hover {
	        background: rgba(224,15,44,0.99);
	    }
	    body.light-layout .social-list ul li a:hover {
	        border-color: rgba(224,15,44,0.99);
	        background: rgba(224,15,44,0.99);
	    }
	    body.light-layout .base-player .content-base-player .buttons .play-pause.pause {
	        background: rgba(224,15,44,0.99);
	    }
	    body.light-layout .base-player .content-base-player .playlist .button-playlist {
	        background: rgba(224,15,44,0.99);
	    }
	    body.light-layout .menu .navbar .navbar-collapse .nav li .sub-menu li a:hover {
	        background: rgba(224,15,44,0.99);
	    }
	    body.light-layout .menu .navbar .navbar-form:hover button {
	        background: rgba(224,15,44,0.99);
	    }
	    body.light-layout .top-albums-widget article .content > a:hover {
	        color: rgba(224,15,44,0.99);
	    }
	    body.light-layout .widget_product_search:hover button[type=submit] {
	        background: rgba(224,15,44,0.99);
	    }
	    body.light-layout .widget_product_search:hover input[type=text] {
	        border-color: rgba(224,15,44,0.99);
	    }
	    body.light-layout .cart input[type=button]:hover {
	        background: rgba(224,15,44,0.99);
	    }
	    body.light-layout .woocommerce .login input[type=submit]:hover,
	    body.light-layout .woocommerce .checkout_coupon input[type=submit]:hover,
	    body.light-layout .woocommerce .lost_reset_password input[type=submit]:hover,
	    body.light-layout .woocommerce .checkout input[type=submit]:hover,
	    body.light-layout .woocommerce form.register input[type=submit]:hover {
	        color: rgba(224,15,44,0.99);
	    }
	    body.light-layout .footer .widget.widget_recent_entries ul li a:hover,
	    body.light-layout .footer .widget.widget_meta ul li a:hover {
	        color: rgba(224,15,44,0.99);
	    }
	    body.light-layout .footer-info .info p a:hover {
	        color: rgba(224,15,44,0.99);
	    }
	    body.light-layout .pagination > li > a:hover,
	    body.light-layout .pagination > li > span:hover,
	    body.light-layout .pagination > li > a:focus,
	    body.light-layout .pagination > li > span:focus {
	        background-color: rgba(224,15,44,0.99);
	    }
	    body.light-layout .pagination > .active > a,
	    body.light-layout .pagination > .active > span,
	    body.light-layout .pagination > .active > a:hover,
	    body.light-layout .pagination > .active > span:hover,
	    body.light-layout .pagination > .active > a:focus,
	    body.light-layout .pagination > .active > span:focus {
	        background-color: rgba(224,15,44,0.99);
	        border-color: rgba(224,15,44,0.99);
	    }
	    body.light-layout .widget_search:hover .icon {
	        background: rgba(224,15,44,0.99);
	    }
	    body.light-layout .widget_search form:hover button {
	        background: rgba(224,15,44,0.99);
	    }
	    body.light-layout .nav-tabs > li.active > a,
	    body.light-layout .nav-tabs > li.active > a:hover,
	    body.light-layout .nav-tabs > li.active > a:focus {
	        border-color: rgba(224,15,44,0.99);
	    }
	    body.light-layout .nav-tabs > li > a:hover,
	    body.light-layout .nav-tabs > li > a:focus {
	        border-color: rgba(224,15,44,0.99);
	    }
	    @media (max-width: 1030px) {
	        body.light-layout .menu .navbar .navbar-toggle:hover,
	        body.light-layout .menu .navbar .navbar-toggle:focus {
	            border-color: rgba(224,15,44,0.99);
	        }
	        body.light-layout .menu .navbar .navbar-collapse .nav li > a:hover {
	            background: rgba(224,15,44,0.99) !important;
	        }
	    }

	    .products > li .product-container .onsale {
	        background: rgba(224,15,44,0.8);
	    }

    </style>


</head>

<body class="home page page-id-59 page-template page-template-template-page-fullwidth-php light-layout wpb-js-composer js-comp-ver-4.2.3 vc_responsive top-slider" data-swfpath="http://demo.stylishthemes.co/clubix/light/wp-content/themes/clubix-v2-final/assets/js">
	
	<!-- ================================================== -->
	<!-- =============== START HEAD CONTAINER ================ -->
	<!-- ================================================== -->
	<section class="head-container">
	    <!-- ================================================== -->
	    <!-- =============== START HEADER ================ -->
	    <!-- ================================================== -->
	    <!-- =============== START MENU ================ -->
	    <!-- ================================================== -->
	        <section class="menu  my-cart-link">
	        <div class="container">
	            <div class="row">
	                <div class="col-sm-12">
	                    <nav class="navbar navbar-default" role="navigation">
	                        <!-- Brand and toggle get grouped for better mobile display -->
	                        <div class="navbar-header">
	                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#clubix-navbar-collapse">
	                                <span class="sr-only">Toggle navigation</span>
	                                <span class="icon-bar"></span>
	                                <span class="icon-bar"></span>
	                                <span class="icon-bar"></span>
	                            </button>
	                        </div>
	                        <!-- Collect the nav links, forms, and other content for toggling -->
	                        <div class="collapse navbar-collapse" id="clubix-navbar-collapse">

	                            <ul id="menu-clubix" class="nav navbar-nav">
								
									<li id="menu-item-469" class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-59 current_page_item menu-item-469 active"><a href="<?php echo base_url(); ?>assets/css/Clubix – Nightlife, Artists, Music & Events WordPress Theme.html">Home</a></li>
									<li id="menu-item-464" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-464"><a href="http://demo.stylishthemes.co/clubix/light/blog-style-1/">Show</a>
									<ul class="sub-menu">
										<li id="menu-item-463" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-463"><a href="http://demo.stylishthemes.co/clubix/light/blog-style-1/">Show 1</a></li>
										<li id="menu-item-462" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-462"><a href="http://demo.stylishthemes.co/clubix/light/blog-style-1-fullwidth/">Show 1 Fullwidth</a></li>
										<li id="menu-item-461" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-461"><a href="http://demo.stylishthemes.co/clubix/light/blog-style-2/">Show 2</a></li>
										<li id="menu-item-460" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-460"><a href="http://demo.stylishthemes.co/clubix/light/blog-style-2-fullwidth/">Show 2 Fullwidth</a></li>
									</ul>
									</li>
									<li id="menu-item-468" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-468"><a href="http://demo.stylishthemes.co/clubix/light/events-style-1/">Listen</a>
									<ul class="sub-menu">
										<li id="menu-item-467" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-467"><a href="http://demo.stylishthemes.co/clubix/light/events-style-1/">Listen Style 1</a></li>
										<li id="menu-item-466" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-466"><a href="http://demo.stylishthemes.co/clubix/light/events-style-2/">Listen Style 2</a></li>
										<li id="menu-item-465" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-465"><a href="http://demo.stylishthemes.co/clubix/light/events-style-3/">Listen Style 3</a></li>
										
									</ul>
									</li>
									<li id="menu-item-474" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-474"><a href="http://demo.stylishthemes.co/clubix/light/albums-style-1/">Video</a>

									</li>
									<li id="menu-item-479" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-479"><a href="http://demo.stylishthemes.co/clubix/light/artists-clubs/">Photo</a>

									</li>
									<li id="menu-item-482" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-482"><a href="http://demo.stylishthemes.co/clubix/light/photo-gallery/">Win</a>

									</li>
									<li id="menu-item-485" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-485"><a href="http://demo.stylishthemes.co/clubix/light/custom-backgrounds/">What's On</a>

									</li>
									<li id="menu-item-452" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-452"><a href="http://demo.stylishthemes.co/clubix/light/shop/">Music</a>
									<ul class="sub-menu">
										<li id="menu-item-484" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-484"><a href="http://demo.stylishthemes.co/clubix/light/cart/">Music Cart</a></li>
										<li id="menu-item-451" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-451"><a href="http://demo.stylishthemes.co/clubix/light/checkout/">Musci Playlist</a></li>
									</ul>
									</li>
									<li id="menu-item-483" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-483"><a href="http://demo.stylishthemes.co/clubix/light/contact/">Entertainment & Lifestyle</a></li>
									<li id="menu-item-483" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-483"><a href="http://demo.stylishthemes.co/clubix/light/contact/">About Us</a></li>
								</ul>
	                            <form action="<?php echo base_url(); ?>assets/css/Clubix – Nightlife, Artists, Music & Events WordPress Theme.html" class="navbar-form navbar-right clearfix" role="search" method="get">
							  		<div class="form-group">
									    <input name="s" id="s" type="text" class="form-control" placeholder="Search..." value="">
									</div>
								</form>                        
							</div>
						</nav>
	                </div>
	            </div>
	        </div>
	    </section>
	    <!-- ================================================== -->
	    <!-- =============== END MENU ================ -->
		<section class="header">
	        <div class="container">
	            <div class="row">
	                <div class="col-sm-12">
	                    <header class="clearfix">
	                        <!-- LOGO ANCHOR -->
	                        <a href="<?php echo base_url(); ?>assets/css/Clubix – Nightlife, Artists, Music & Events WordPress Theme.html" class="logo">
	                            <!-- LOGO -->
	                            <img src="<?php echo base_url(); ?>assets/img/logo.png" alt="Clubix – Nightlife, Artists, Music &amp; Events WordPress Theme">
	                        </a>
	                        <!-- SOCIAL MEDIA LIST -->
	                        <nav class="social-list clearfix">
	                            <ul>
	                                <li><a href="http://demo.stylishthemes.co/clubix/light/#" target="_blank"><i class="fa fa-facebook"></i></a></li><li><a href="http://demo.stylishthemes.co/clubix/light/#" target="_blank"><i class="fa fa-tumblr"></i></a></li><li><a href="http://demo.stylishthemes.co/clubix/light/#" target="_blank"><i class="fa fa-twitter"></i></a></li>                            </ul>
	                        </nav>
	                    </header>
	                </div>
	            </div>
	        </div>
	    </section>
	    <!-- ================================================== -->
	    <!-- =============== END HEADER ================ -->
	    <!-- ================================================== -->
	    <!-- ================================================== -->
	    
	    <!-- ================================================== -->
	</section>
	<!-- ================================================== -->
	<!-- =============== END HEAD CONTAINER ================ -->
	<!-- ================================================== -->