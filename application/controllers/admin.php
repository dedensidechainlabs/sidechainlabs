<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

ini_set('max_execution_time', 0);

class Admin extends CI_Controller {

	function __construct()
    {
        parent::__construct();
        $this->load->model('mdata');
		$this->load->helper(array('form', 'url','file'));
	}
	
	function index()
	{
		$data['title'] = "Korean McSpicy";
		$data['description'] = "Korean McSpicy";
		$data['keywords'] = "Korean McSpicy";
		$this->load->view('vadminlogin',$data);
	}	

	function login()
	{
		$txtusername = $this->input->post("txtusername");
		$txtpassword = md5($this->input->post("txtpassword"));
		$qcekusername = $this->db->query("SELECT * FROM admin WHERE username='$txtusername' AND password='$txtpassword'");
		$valcekusername = $qcekusername->num_rows();
		if($valcekusername == 1){
			$row = $qcekusername->row();
			$userlevel = $row->level;
			$this->load->library('session');
			$this->session->set_userdata('username', $txtusername);
			$this->session->set_userdata('userlevel', $userlevel);
			redirect('admin/dashboard/','refresh');
		}else{
			$data['title'] = "Korean McSpicy";
			$data['description'] = "Korean McSpicy";
			$data['keywords'] = "Korean McSpicy";
			$data['error'] = "<h5 style='color:#ff0000;'>Invalid Username or Password!<br>Please Try Again!</h5>";
			$this->load->view('vadminlogin',$data);
		}
	}

	function dashboard()
	{
		if($this->session->userdata('username')!='' || $this->session->userdata('username')!=NULL){
			$data['title'] = "Korean McSpicy";
			$data['description'] = "Korean McSpicy";
			$data['keywords'] = "Korean McSpicy";
			$data['username'] = $this->session->userdata('username');
			$this->load->view('vadmindashboard',$data);
		}else{
			redirect('mcd-mcspicy-admin','refresh');
		}
	}

	function users()
	{
		if($this->session->userdata('username')!='' || $this->session->userdata('username')!=NULL){
			$data['title'] = "Korean McSpicy";
			$data['description'] = "Korean McSpicy";
			$data['keywords'] = "Korean McSpicy";
			$data['username'] = $this->session->userdata('username');
			$data['getUsers'] = $this->mdata->getUsers();
			$this->db->from('users');
			$data['getUsersNumRows'] = $this->db->get()->num_rows();
			$this->load->view('vadminusers',$data);
		}else{
			redirect('mcd-mcspicy-admin','refresh');
		}
	}

	function videos()
	{
		if($this->session->userdata('username')!='' || $this->session->userdata('username')!=NULL){
			$data['title'] = "Korean McSpicy";
			$data['description'] = "Korean McSpicy";
			$data['keywords'] = "Korean McSpicy";
			$data['username'] = $this->session->userdata('username');
			$data['getVideos'] = $this->mdata->getVideos();
			$this->db->from('video');
			$data['getVideosNumRows'] = $this->db->get()->num_rows();
			$this->load->view('vadminvideos',$data);
		}else{
			redirect('mcd-mcspicy-admin','refresh');
		}
	}

	function uploadvideochoose()
	{
		if($this->session->userdata('username')!='' || $this->session->userdata('username')!=NULL){
			$data['title'] = "Korean McSpicy";
			$data['description'] = "Korean McSpicy";
			$data['keywords'] = "Korean McSpicy";
			$data['username'] = $this->session->userdata('username');
			$id = $this->input->post('txtvideoid');
			$data['getVideoDetail'] = $this->mdata->getVideoDetailByVideoID($id);
			$this->load->view('vadminuploadvideodetail',$data);
		}else{
			redirect('mcd-mcspicy-admin','refresh');
		}
	}

	function do_upload()
	{
		$time = time();
		$fileName = $this->session->userdata('fileName');
    	$fileTempName = 'assets/upload/'.$fileName;
    	$rawName = substr($fileName, 0, strrpos($fileName, '.'));
    	
		if (S3::putObjectFile($fileTempName, "demo.learning", $fileName, S3::ACL_PUBLIC_READ)) {
		    //echo "We successfully uploaded your file.";
		    
		    try{
		    	$zencoder = new Services_Zencoder('ccd5fe2e1a8d3f8e55d2efcdfb06b9c4');
		    	// New Encoding Job
				$encoding_job = $zencoder->jobs->create(
					array(
					  "input" => "s3://demo.learning/".$fileName,
					  "outputs" => array(
					    array(
							"public" => "true",
							"label" => "web",
							"url"=> "s3://demo.learning.output/".$rawName.$time.".mp4",
							"notifications" => array(
								"hermantaniwan@gmail.com",
							  	"http://hermantaniwan.com/aws/notification.php"
							)
					    )
					  )
					)
				);
				
				// Success if we got here
				//echo "w00t! \n\n";
				//echo "Job ID: ".$encoding_job->id."\n";
				//echo "Output ID: ".$encoding_job->outputs['web']->id."\n";
				// Store Job/Output IDs to update their status when notified or to check their progress.
				$txtID = $this->input->post('txtID');
				$videoName = $rawName.$time.'.mp4';
				$uploadDate = date('Y-m-d H:i:s');
				$dataVideo = array(
					'link' => $videoName,
					'publish' => 1,
					'created_at' => $uploadDate
				);
				$this->db->where('id', $txtID);
				$this->db->update('video', $dataVideo); 
				
				redirect('admin/videos/','refresh');
				
			} catch (Services_Zencoder_Exception $e) {
				// If were here, an error occured
				echo "Fail :(\n\n";
				echo "Errors:\n";
				foreach ($e->getErrors() as $error) echo $error."\n";
				echo "Full exception dump:\n\n";
				print_r($e);
			}

					
		}else{
		    echo "Something went wrong while uploading your file... sorry.";
		}
	}

	function setting()
	{
		if($this->session->userdata('username')!='' || $this->session->userdata('username')!=NULL){
			$data['title'] = "Korean McSpicy";
			$data['description'] = "Korean McSpicy";
			$data['keywords'] = "Korean McSpicy";
			$data['username'] = $this->session->userdata('username');
			$query = $this->db->get_where('config', array('setting' => 'maintenance-mode'))->row();
			$data['getMaintenanceMode'] =  $query->value;

			
			$this->load->view('vadminsetting',$data);
		}else{
			redirect('mcd-mcspicy-admin','refresh');
		}
	}

	function userdetail()
	{
		if($this->session->userdata('username')!='' || $this->session->userdata('username')!=NULL){
			$data['title'] = "Korean McSpicy";
			$data['description'] = "Korean McSpicy";
			$data['keywords'] = "Korean McSpicy";
			$data['username'] = $this->session->userdata('username');
			$id = $this->input->get('id',true);
			$data['getVideoDetail'] = $this->mdata->getVideoDetail($id);
			$query = $this->db->get_where('video', array('user_id' => $id));
			$data['getVideoDetailNumRows'] = $query->num_rows();
			$this->load->view('vadminuserdetail',$data);
		}else{
			redirect('mcd-mcspicy-admin','refresh');
		}
	}

	function changemode()
	{
		if($this->session->userdata('username')!='' || $this->session->userdata('username')!=NULL){
			$data['title'] = "Korean McSpicy";
			$data['description'] = "Korean McSpicy";
			$data['keywords'] = "Korean McSpicy";
			$data['username'] = $this->session->userdata('username');
			
			$settingName = 'maintenance-mode';
			$settingValue = $this->input->post('maintenanceMode');
			$dataSetting = array(
				'value' => $settingValue
			);
			$this->mdata->saveMaintenanceMode($settingName, $dataSetting);
			redirect('admin/setting','refresh');
		}else{
			redirect('mcd-mcspicy-admin','refresh');
		}
	}

	function changepublish()
	{
		if($this->session->userdata('username')!='' || $this->session->userdata('username')!=NULL){
			$id = $this->input->post("txtid");
			$publish = $this->input->post('txtpublish');
			$dataVideo = array(
				'publish' => $publish
			);
			$this->mdata->updateVideo($id, $dataVideo);
			redirect('admin/videos','refresh');
		}else{
			redirect('mcd-mcspicy-admin','refresh');
		}
	}

	function prize()
	{
		$data['title'] = "Korean McSpicy";
		$data['description'] = "Korean McSpicy";
		$data['keywords'] = "Korean McSpicy";
		$data['username'] = $this->session->userdata('username');
		$this->db->from('config');
		$this->db->where('setting','prize');
		$query = $this->db->get()->row();
		$data['prizeValue'] = $query->value;
		$this->load->view('vadminprize',$data);
	}

	function uploadPrize()
	{
		$images = $this->input->post('userfile');
				
		$config['upload_path'] = './assets/img/prize/';
		$config['allowed_types'] = 'jpg|png|pdf';
		$config['max_size']	= '3000';
		$config['max_width']  = '2024';
		$config['max_height']  = '2068';
		
		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		if ( ! $this->upload->do_upload("userfile"))
		{
			$error = array('error' => $this->upload->display_errors());
			$this->load->view('vadminprize',$error);
		}else{
			$upload_data = $this->upload->data(); 
			$file_name =   $upload_data['file_name'];
			$value = array(
				'value' => $file_name
			);
			$setting = 'prize';
			$this->mdata->updateSetting($setting, $value);
			redirect('admin/prize','refresh');
		}
	}

	function tnc()
	{
		$data['title'] = "Korean McSpicy";
		$data['description'] = "Korean McSpicy";
		$data['keywords'] = "Korean McSpicy";
		$data['username'] = $this->session->userdata('username');
		$this->db->from('config');
		$this->db->where('setting','tnc');
		$query = $this->db->get()->row();
		$data['tncValue'] = $query->value;
		$this->load->view('vadmintnc',$data);
	}

	function saveTnc()
	{
		$txtTnc = $this->input->post('txtTnc');
		$value = array(
				'value' => $txtTnc
			);
		$setting = 'tnc';
		$this->mdata->updateSetting($setting, $value);
		redirect('admin/tnc','refresh');
	}

	function pvp()
	{
		$data['title'] = "Korean McSpicy";
		$data['description'] = "Korean McSpicy";
		$data['keywords'] = "Korean McSpicy";
		$data['username'] = $this->session->userdata('username');
		$this->db->from('config');
		$this->db->where('setting','pvp');
		$query = $this->db->get()->row();
		$data['pvpValue'] = $query->value;
		$this->load->view('vadminpvp',$data);
	}

	function savePvp()
	{
		$txtPvp = $this->input->post('txtPvp');
		$value = array(
				'value' => $txtPvp
			);
		$setting = 'pvp';
		$this->mdata->updateSetting($setting, $value);
		redirect('admin/pvp','refresh');
	}

	function logout(){
		$this->session->sess_destroy();
		redirect('mcd-mcspicy-admin','refresh');
	}
}